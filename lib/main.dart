import 'package:egg/utility/app_theme.dart';
import 'package:flutter/material.dart';

import 'log.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

          primarySwatch: AppTheme.SEAHORSE_PRIMARY_MAT,
          fontFamily: AppTheme.fontName
      ),
      home: const LogEgg(),
    );
  }
}

