import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();


  static const MaterialColor SEAHORSE_PRIMARY_MAT =  MaterialColor(0xFFe26c64 ,  const <int, Color>{
    50: const Color(0xFFe26c64  ),//10%
    100: const Color(0xFFe26c64 ),//20%
    200: const Color(0xFFe26c64 ),//30%
    300: const Color(0xFFe26c64 ),//40%
    400: const Color(0xFFe26c64 ),//50%
    500: const Color(0xFFe26c64 ),//60%
    600: const Color(0xFFe26c64 ),//70%
    700: const Color(0xFFe26c64 ),//80%
    800: const Color(0xFFe26c64 ),//90%
    900: const Color(0xFFe26c64),//100%
  });



  static const Color SPRING_PRIMARY = Color(0xFFe26c64);
  static const Color SEAHORSE_PRIMARY  = Color(0xFF5D6AE0);
  static const Color SEAHORSE_RED  = Color(0xFFFF0000);
  static const Color SEAHORSE_WHITE_FF = Color(0xFFFFFFFF);
  static const Color SEAHORSE_WHITE_FD = Color(0xFFFDFDFD);

  static const Color SEAHORSE_GREY_70 = Color(0xFF707070);
  static const Color SEAHORSE_WHITE_A8 = Color(0xFFA8A8A8);
  static const Color SEAHORSE_WHITE_66 = Color(0xFF666666);




  static const Color cardWait = Color(0xFF984855);
  static const Color cardSuccess = Color(0xFF4A8A71);
  static const Color appbarMain = Color(0xFF303340);
  static const Color  badge = Color(0xFFFB2E2D);
  static const Color appBarText = Color(0xFF303340);

  static const Color bg = Color(0xFF24283B);


  static const Color green = Color(0xFF00CF0E);
  static const String fontName = 'Prompt';

}


