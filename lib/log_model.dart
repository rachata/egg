class LogModel {
  int? status;
  List<Message>? message;
  List<Log>? log;

  LogModel({this.status, this.message, this.log});

  LogModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = <Message>[];
      json['message'].forEach((v) {
        message!.add(new Message.fromJson(v));
      });
    }
    if (json['log'] != null) {
      log = <Log>[];
      json['log'].forEach((v) {
        log!.add(new Log.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message!.map((v) => v.toJson()).toList();
    }
    if (this.log != null) {
      data['log'] = this.log!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  String? n1;
  String? n2;
  String? n3;

  Message({this.n1, this.n2, this.n3});

  Message.fromJson(Map<String, dynamic> json) {
    n1 = json['n1'];
    n2 = json['n2'];
    n3 = json['n3'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['n1'] = this.n1;
    data['n2'] = this.n2;
    data['n3'] = this.n3;
    return data;
  }
}

class Log {
  int? number1;
  int? number2;
  int? number3;
  int? eggStation;
  String? dt;

  Log({this.number1, this.number2, this.number3, this.eggStation, this.dt});

  Log.fromJson(Map<String, dynamic> json) {
    number1 = json['number1'];
    number2 = json['number2'];
    number3 = json['number3'];
    eggStation = json['egg_station'];
    dt = json['dt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number1'] = this.number1;
    data['number2'] = this.number2;
    data['number3'] = this.number3;
    data['egg_station'] = this.eggStation;
    data['dt'] = this.dt;
    return data;
  }
}