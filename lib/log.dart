import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:dio/dio.dart';
import 'package:egg/log_model.dart';
import 'package:egg/utility/app_theme.dart';
import 'package:egg/utility/config.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart' as xlsio;

class LogEgg extends StatefulWidget {
  const LogEgg({Key? key}) : super(key: key);

  @override
  _LogEggState createState() => _LogEggState();
}

class _LogEggState extends State<LogEgg> with AfterLayoutMixin<LogEgg> {
  TextEditingController dtStart = TextEditingController();
  TextEditingController dtEnd = TextEditingController();

  int n1 = 0;
  int n2 = 0;
  int n3 = 0;
  int sum = 0;


  late LogModel  logs;

  late DateTime _dtStart;
  late DateTime _dtEnd;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF000080),
        title: Text("Number of Egg"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
                padding: EdgeInsets.only(left: 10, top: 10),
                child: GestureDetector(
                  onTap: () => selectDateTime(true),
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: Text(
                          "Start Date-Time: ",
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      textField(50, (MediaQuery.of(context).size.width * 0.6),
                          click: true,
                          controller: dtStart,
                          isStart: true,
                          readOnly: true),
                    ],
                  ),
                )),
            Padding(
                padding: EdgeInsets.only(left: 10, top: 10),
                child: GestureDetector(
                  onTap: () => selectDateTime(true),
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: Text(
                          "End Date-Time : ",
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      textField(50, (MediaQuery.of(context).size.width * 0.6),
                          click: true,
                          controller: dtEnd,
                          isStart: false,
                          readOnly: true),
                    ],
                  ),
                )),
            Padding(
              padding: EdgeInsets.only(left: 10, top: 2, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 150,
                    child: RaisedButton.icon(
                      onPressed: () {
                        callAPI();
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      label: Text(
                        'Search',
                        style: TextStyle(color: Colors.white),
                      ),
                      icon: Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                      textColor: Colors.white,
                      splashColor: Colors.red,
                      color: AppTheme.SPRING_PRIMARY,
                    ),
                  ),
                  SizedBox(width: 30,),
                  Container(
                    width: 150,
                    child: RaisedButton.icon(
                      onPressed: () {
                        export();
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(10.0))),
                      label: Text(
                        'Export',
                        style: TextStyle(color: Colors.white),
                      ),
                      icon: Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                      textColor: Colors.white,
                      splashColor: Colors.red,
                      color: AppTheme.SPRING_PRIMARY,
                    ),
                  )
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(left: 30, right: 30),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.7,
                child: GridView.builder(
                    physics: BouncingScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 180,
                        crossAxisSpacing: 20,
                        mainAxisSpacing: 20),
                    itemCount: 4,
                    itemBuilder: (BuildContext ctx, index) {
                      return GestureDetector(
                        onTap: () => {




                        },
                        child: Container(
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // LayoutBuilder(
                              //   builder: (context, constraints) {
                              //     return ImageIcon(
                              //       AssetImage(menuS[index].imgPath),
                              //       color: Colors.white,
                              //       size: constraints.maxWidth * 0.5,
                              //     );
                              //   },
                              // ),
                              SizedBox(
                                height: 10,
                              ),
                              index == 0 ? Text(
                                "Jumbo (0)",
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ) : index == 1 ? Text(
                                "Extra Large (1)",
                                textAlign: TextAlign.center ,
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Colors.white,

                                    fontWeight: FontWeight.w600),
                              ) : index == 2 ? Text(
                                "Large (2)",
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ) : Text(
                                "Total",
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ),
                              index == 0 ?  Text(
                                "${n1}",
                                style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ) : index == 1 ?  Text(
                                "${n2}",
                                style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              )  :  index == 2 ?  Text(
                                "${n3}",
                                style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ) :  Text(
                                "${sum}",
                                style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Color(0xFF4e1786) ,
                                  Color(0xFF000080),

                                ],
                              ),
                              borderRadius: BorderRadius.circular(15)),
                        ),
                      );
                    }),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(left: 10, top: 2, right: 10),
              child: Divider(
                thickness: 1,
                color: Colors.grey.withOpacity(0.5),
              ),
            ),

          ],
        ),
      ),
    );
  }

  textField(double H, double W,
      {bool readOnly = false,
      bool click = false,
      TextEditingController? controller,
      bool isStart = true}) {
    return Container(
        margin: EdgeInsets.only(left: 10, right: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(5.0),
              bottomRight: Radius.circular(5.0),
              topLeft: Radius.circular(5.0),
              bottomLeft: Radius.circular(5.0)),
        ),
        width: W * 0.9,
        height: 35,
        child: Align(
          alignment: Alignment.center,
          child: Theme(
            data: new ThemeData(
              primaryColor: Colors.redAccent,
              primaryColorDark: Colors.red,
            ),
            child: new TextField(
              controller: controller,
              onTap: () => {
                if (click) {selectDateTime(isStart)}
              },
              readOnly: readOnly,
              keyboardType: TextInputType.number,
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontFamily: AppTheme.fontName,
                  color: Color(0xFF333333),
                  fontSize: 12),
              decoration: new InputDecoration(
                contentPadding: EdgeInsets.only(left: 10),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide:
                      BorderSide(color: AppTheme.SPRING_PRIMARY, width: 2.0),
                ),
                border: new OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(2.0)),
                    borderSide: new BorderSide(color: Colors.teal)),
              ),
            ),
          ),
        ));
  }

  selectDateTime(bool isStart) async {
    try {
      final DateTime? date = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(2022, 3),
          lastDate: DateTime(2200),
          builder: (BuildContext context, Widget? child) {
            return MediaQuery(
                data: MediaQuery.of(context).copyWith(
                    // Using 24-Hour format
                    alwaysUse24HourFormat: true),
                // If you want 12-Hour format, just change alwaysUse24HourFormat to false or remove all the builder argument
                child: child!);
          });

      TimeOfDay now = TimeOfDay.now();

      final TimeOfDay? time = await showTimePicker(
          context: context,
          initialTime: TimeOfDay(hour: now.hour, minute: now.minute),
          builder: (context, Widget? child) {
            return MediaQuery(
                data: MediaQuery.of(context).copyWith(
                    // Using 24-Hour format
                    alwaysUse24HourFormat: true),
                // If you want 12-Hour format, just change alwaysUse24HourFormat to false or remove all the builder argument
                child: child!);
          });

      if (isStart) {
        _dtStart = new DateTime(
            date!.year, date.month, date.day, time?.hour ?? 0, time!.minute);

        String formattedDate = DateFormat('dd-MM-yyyy HH:mm').format(_dtStart!);

        setState(() {
          dtStart.text = formattedDate;
        });
      } else {
        _dtEnd = new DateTime(
            date!.year, date.month, date.day, time?.hour ?? 0, time!.minute);

        String formattedDate = DateFormat('dd-MM-yyyy HH:mm').format(_dtEnd!);

        setState(() {
          dtEnd.text = formattedDate;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  initCallAPI() async {





    print("erederer");
    try {
      String formattedDateShowStart =
          DateFormat('yyyy-MM-dd 00:00:00').format(DateTime.now());
      String formattedDateGetEnd =
          DateFormat('yyyy-MM-dd 23:59:59').format(DateTime.now());

      dtStart.text = formattedDateShowStart;
      dtEnd.text = formattedDateGetEnd;

      var response = await Dio().get(
          '${ConfigServer.serverHTTP}/log/${formattedDateShowStart}/${formattedDateGetEnd}');



      setState(() {
          logs = LogModel.fromJson(response.data);
      });

      setState(() {
        n1 = int.parse(response.data['message'][0]['n1']);
        n2 = int.parse(response.data['message'][0]['n2']);
        n3 = int.parse(response.data['message'][0]['n3']);

        sum = n1 + n2 + n3;

        print(n1);
      });
    } catch (e) {}



  }

  callAPI() async {


    ProgressDialog? pd;

    pd = ProgressDialog(context: context);
    pd!.show(max: 50, msg: 'please wait..');

    try {
      String formattedDateShowStart =
          DateFormat('yyyy-MM-dd HH:mm:00').format(_dtStart!);
      String formattedDateGetEnd =
          DateFormat('yyyy-MM-dd HH:mm:00').format(_dtEnd!);


      print("${ConfigServer.serverHTTP}/log/${formattedDateShowStart}/${formattedDateGetEnd}");

      var response = await Dio().get(
          '${ConfigServer.serverHTTP}/log/${formattedDateShowStart}/${formattedDateGetEnd}');
      print(response);

      setState(() {
        n1 = int.parse(response.data['message'][0]['n1']);
        n2 = int.parse(response.data['message'][0]['n2']);
        n3 = int.parse(response.data['message'][0]['n3']);

        sum = n1 + n2 + n3;
      });

      setState(() {
        logs = LogModel.fromJson(response.data);
      });

    } catch (e) {
      print(e);
    }

    pd!.close();

  }

  @override
  void initState() {
    initCallAPI();
  }

  void export() async{

    ProgressDialog? pd;

    pd = ProgressDialog(context: context);
    pd!.show(max: 50, msg: 'please wait..');

    print("deded");

    final xlsio.Workbook workbook = xlsio.Workbook(0);

    final xlsio.Worksheet sheet1 = workbook.worksheets.addWithName('Log');

    sheet1.getRangeByName("A1:A1").columnWidth = 20;
    sheet1.getRangeByName("B1:B1").columnWidth = 20;
    sheet1.getRangeByName("C1:C1").columnWidth = 20;
    sheet1.getRangeByName("D1:D1").columnWidth = 20;
    sheet1.getRangeByName("E1:E1").columnWidth = 60;

    final xlsio.Style style1 = workbook.styles.add('Style1');
    style1.backColor = '#D9E1F2';
    style1.hAlign = xlsio.HAlignType.center;
    style1.vAlign = xlsio.VAlignType.center;
    style1.bold = true;


    sheet1.getRangeByIndex(1, 1).text = 'Jambo (0)';
    sheet1.getRangeByIndex(1, 2).text = 'Extra Large (1)';
    sheet1.getRangeByIndex(1, 3).text = 'Large (2)';
    sheet1.getRangeByIndex(1, 4).text = 'Sum';
    sheet1.getRangeByIndex(1, 5).text = 'Date-Time';



  int  i = 2;


    for (int j = 0; j < logs.log!.length; j++) {
      print(j);

      sheet1
          .getRangeByName('A${i}')
          .setText("${logs.log![j].number1}");
      sheet1
          .getRangeByName('B${i}').setText("${logs.log![j].number2}");
      sheet1
          .getRangeByName('C${i}').setText("${logs.log![j].number3}");
      sheet1
          .getRangeByName('D${i}').setText("${logs.log![j].number1! + logs.log![j].number2! +logs.log![j].number3!}");
      sheet1
          .getRangeByName('E${i}').setText("${logs.log![j].dt}");

      i++;
    }




    final List<int> bytes = workbook.saveAsStream();
    workbook.dispose();



    File file =
        File('${(await getTemporaryDirectory()).path}/log${dtStart.text} - ${dtEnd.text}.xlsx');

    await file.writeAsBytes(bytes, flush: true);

    pd!.close();

    await OpenFile.open(file.path);


  }

  @override
  void afterFirstLayout(BuildContext context) {

  }
}

class DataLog {
  DataLog(this.number, this.count);

  final String number;
  final int count;
}
